# http://peterdowns.com/posts/first-time-with-pypi.html
from distutils.core import setup
setup(
  name = 'mycelia',
  packages = ['mycelia'], # this must be the same as the name above
  version = '0.1',
  description = '',
  author = 'Millicent Billette',
  author_email = 'millicent.b@agatas.org',
  url = 'https://framagit.org/mycelia/mycelia/', # use the URL to the github repo
  download_url = 'https://framagit.org/mycelia/mycelia/archive/0.1.tar.gz', # I'll explain this in a second
  keywords = ['mycelia', 'data-viz'], # arbitrary keywords
  classifiers = [],
)